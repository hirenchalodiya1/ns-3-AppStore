# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.apps import apps
from apps.documents import AppDocument, TagDocument


def findTags():
    min_tag_count = 3
    num_of_top_tags = 20
    tag_cloud_max_font_size_em = 2.0
    tag_cloud_min_font_size_em = 1.0
    tag_cloud_delta_font_size_em = tag_cloud_max_font_size_em - tag_cloud_min_font_size_em

    Tag = apps.get_model('apps', 'Tag')
    top_tags = Tag.objects.all()
    not_top_tags = []

    return top_tags, not_top_tags


def search(request):
    App = apps.get_model('apps', 'App')
    Tag = apps.get_model('apps', 'Tag')
    top_tags, not_top_tags = findTags()
    query = request.GET.get('q', '')
    sqs_app = AppDocument.search().query("multi_match", query = query, fields = ["name", "title", "abstract", "description"])
    sqs_tag = TagDocument.search().query("match", name = query)
    app = sqs_app.to_queryset()
    tag = sqs_tag.to_queryset()    
        
    context = {
        'top_tags': top_tags,
        'not_top_tags': not_top_tags,
        'sqs_app': app,
        'sqs_tag': tag,
    }
    return render(request, 'search.html', context)
